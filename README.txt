A systemic functional grammar of French interrogative sentences.

The grammar (FRENCH/) was implemented using KPML[1]. For more information, see
docs.tex and sketch.txt.

This project is licensed under the terms of the EUPL v1.1.

[1] http://www.fb10.uni-bremen.de/anglistik/langpro/kpml/README.html

--
Tim Nieradzik
timn@uni-bremen.de
