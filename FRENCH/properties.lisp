(in-package "KPML")

;;; File creation information: 
;;; 12-2-2012 15:04:29 at Unknown.




(define-language-morphology-requirements :language :FRENCH
    :systemicized NIL
    :generator-fn FRENCH-GRAMMATICAL-FEATURES-TO-LEXICAL-FEATURE
    )

(define-language-standard-defaults
  :language :FRENCH :defaults NIL)

(define-language-standard-abbreviation
  :language :FRENCH :abbreviation FR)

(define-string-clean-ups
  :language :FRENCH 
  :clean-ups (("," " & *[*") (" " "*[* ") ("" "*[*") ("and" "&") ("," ",,") ("." ",.") ("." ".,") ("?" "?.") ("!" "!.") ("." " .") ("," " ,") (" " "  ") ("?" " ?") ("!" " !")))